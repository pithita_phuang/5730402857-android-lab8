package com.example.pithitamacbook.commuactivityintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_Name, tv_MainResult;
    EditText edt_Name;
    Button btn_OK;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_Name = (TextView)findViewById(R.id.tvName);
        tv_MainResult = (TextView) findViewById(R.id.tvMainResult);
        edt_Name = (EditText)findViewById(R.id.edtName);
        btn_OK = (Button)findViewById(R.id.btnOK);

        btn_OK.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnOK){
            Intent intent = new Intent(getApplicationContext(),ResultActivity.class);
            intent.putExtra("dataName" , edt_Name.getText().toString());
            startActivityForResult(intent,2);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == 2) {
                String returnString = data.getStringExtra("MESSAGE");
                tv_MainResult.setText(returnString);

            }
        }
    }

