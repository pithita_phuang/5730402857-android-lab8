package com.example.pithitamacbook.commuactivityintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView tv_Result;
    EditText edt_Result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        tv_Result = (TextView) findViewById(R.id.tvResult);
        edt_Result = (EditText) findViewById(R.id.edtResult);
        String text = getIntent().getExtras().getString("dataName");
        tv_Result.setText(text);
        //finish();

    }

    @Override
    public void onBackPressed() {

        String message = edt_Result.getText().toString();
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        intent.putExtra("MESSAGE",message);
        setResult(2,intent);
        super.onBackPressed();
    }

//    @Override public void finish(){
//        String message = edt_Result.getText().toString();
//        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
//        intent.putExtra("MESSAGE",message);
//        setResult(2,intent);
//        super.finish();
//    }
}
