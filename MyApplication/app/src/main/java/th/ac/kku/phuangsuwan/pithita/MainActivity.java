package th.ac.kku.phuangsuwan.pithita;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    AppCompatTextView tv_res;
    AppCompatButton btnCal;
    Float num1, num2, result;
    AppCompatRadioButton add_rb, sub_rb, mul_rb, div_rb;
    RadioGroup rgOperator;
    SwitchCompat of_switch;
    Boolean status = false;
    AppCompatEditText editText1,editText2;
    private AppCompatTextView mTxtSwitch;
    long time;
    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_res = (AppCompatTextView) findViewById(R.id.tv_result);
        btnCal = (AppCompatButton) findViewById(R.id.cal);
        add_rb = (AppCompatRadioButton) findViewById(R.id.rb_add);
        sub_rb = (AppCompatRadioButton) findViewById(R.id.rb_sub);
        mul_rb = (AppCompatRadioButton) findViewById(R.id.rb_mul);
        div_rb = (AppCompatRadioButton) findViewById(R.id.rb_div);
        rgOperator = (RadioGroup) findViewById(R.id.rgroup);
        of_switch = (SwitchCompat) findViewById(R.id.sw);
        editText1 = (AppCompatEditText) findViewById(R.id.edt1);
        editText2 = (AppCompatEditText) findViewById(R.id.edt2);
        mTxtSwitch = (AppCompatTextView) findViewById(R.id.txtSwitch) ;
        menu = (Menu) findViewById(R.id.menu);


        btnCal.setOnClickListener(this);

        of_switch.setOnCheckedChangeListener(this);


        rgOperator.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if (status)
                    acceptNumber();
            }
        });

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        showToast("Width = " + width + ", Height = " + height);

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
        if(isCheck){
            status = true;
            mTxtSwitch.setText("ON");
        }
        else{
            status = false;
            mTxtSwitch.setText("OFF");
        }

    }



    @Override
    public void onClick(View view) {
        if(view == btnCal){
            if(status) {
                acceptNumber();

            }

        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu) {
            Toast.makeText(MainActivity.this, "Choose action setting", Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void acceptNumber() {
        time = System.currentTimeMillis();
        Log.d("Computation time = ",  "0.0");


        String st1 = editText1.getText().toString();
        String st2 = editText2.getText().toString();
        try {
            num1 = Float.parseFloat(st1);
            num2 = Float.parseFloat(st2);

            calculate(rgOperator.getCheckedRadioButtonId());


        }catch (IllegalArgumentException e){
            showToast("Please enter only a number");

        }




    }


    private void showToast(String msg) {
        Toast.makeText(MainActivity.this,msg,Toast.LENGTH_LONG).show();

    }

    private void calculate(int i) {


        switch (i) {
            case R.id.rb_add:
                result = num1 + num2;
                break;
            case R.id.rb_sub:
                result = num1 - num2;
                break;
            case R.id.rb_mul:
                result = num1 * num2;
                break;
            case R.id.rb_div:
                if (num2 == 0) {
                    result =  0.0f;
                    showToast("Please divide by a non-zero number");}
                else {
                    result = num1 / num2;
                }
                break;
        }


        tv_res.setText("= " + result);
        Log.d("Computation time = ",  "0." + (System.currentTimeMillis() - time));

    }


}

